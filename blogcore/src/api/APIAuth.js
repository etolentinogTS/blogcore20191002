import API from './API';
import Notification from 'react-bulma-notification';

class APIAuth {
	login = async (loginvm) => {
		let ok = false;
		await API.post('auth/login', loginvm)
			.then((res) => {
				ok = true;
				localStorage.setItem('@BlogCore.token', res.data.token);
				Notification.success('Sesión iniciada exitosamente', { duration: 5 });
			})
			.catch((error) => {
				ok = false;
				Notification.error(error.response.data.Message, { duration: 15 });
				//alert(error.response.data.Message);
			});

		return ok;
	};

	consultarUsuarioSesion = async () => {
		let usuario;

		await API.get('auth/consultarUsuarioSesion')
			.then((res) => {
				usuario = res.data;
			})
			.catch((error) => {
                usuario = null;
                if(error.response.data){
                    Notification.error(error.response.data.Message, { duration: 15 });
                }
                else{
                    Notification.info('Debes iniciar sesión', { duration: 15 });
                }
			});

		return usuario;
	};
}

export default APIAuth;

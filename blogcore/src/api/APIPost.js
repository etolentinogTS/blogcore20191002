import API from './API';

class APIPost {
    consultarTodos = async () => {
        let posts;

		await API.get('post/consultarTodos')
			.then((res) => {
				posts = res.data;
			})
			.catch((error) => {
                posts = [];
                if(error.response.data){
                    Notification.error(error.response.data.Message, { duration: 15 });
                }
                else{
                    Notification.info('Debes iniciar sesión', { duration: 15 });
                }
			});

		return posts;
    }
}

export default APIPost;
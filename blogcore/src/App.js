import React, {Fragment} from 'react';
import 'bulma/css/bulma.css';
import 'react-bulma-notification/build/css/index.css';
import Login from './components/Login';
import Default from './components/Default';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Sesion from './components/Sesion';
import ConsultarPost from './components/ConsultarPost';

function App() {
  return (
    <Router>
      <Fragment>
        <Sesion />
        <Switch>
          <Route exact path="/" component={Default} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/posts" component={ConsultarPost} />
        </Switch>
      </Fragment>
    </Router>
  );
}

export default App;

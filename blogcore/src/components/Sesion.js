import React, { Component } from 'react';
import APIAuth from '../api/APIAuth';
import { Redirect, withRouter } from 'react-router-dom';

export class Sesion extends Component {
	state = {
		usuario: null,
		loading: true
	};

	apiAuth = new APIAuth();

	setStateAsync = (state) => {
		return new Promise((resolve) => {
			this.setState(state, resolve);
		});
	};

	async componentWillMount() {
		const usuarioSesion = await this.consultarUsuarioSesion();

		await this.setStateAsync({ usuario: usuarioSesion, loading: false });
	}

	consultarUsuarioSesion = async () => {
		return await this.apiAuth.consultarUsuarioSesion();
	};

	render() {
		const usuarioSesion = this.state.usuario;
        const { loading } = this.state;
        
        //console.log("ruta", this.props.location.pathname)
		// console.log("sesion", usuarioSesion)+
		// console.log("loading", loading);

		// //ver la url
		// //si es login y usuario en sesión tiene un valor, entonces ve a default
		// console.log("validación1", usuarioSesion === null && loading);
        // console.log("validación2", usuarioSesion!== null && !loading)
        
        if(usuarioSesion !== null && this.props.location.pathname === '/login'){
            return <Redirect to="/" />
        }

		return usuarioSesion === null && loading ? (
			''
		) : usuarioSesion !== null && !loading ? (
			''
		) : (
			<Redirect to="/login" />
		);
	}
}

export default withRouter(Sesion);

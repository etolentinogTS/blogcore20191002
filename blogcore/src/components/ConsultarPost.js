import React, { Component, Fragment } from 'react';
import APIPost from '../api/APIPost';

export class ConsultarPost extends Component {
	state = {
		posts: []
	};

	apiPost = new APIPost();

	async componentDidMount() {
		//consultar los post
		this.setState({
			posts: await this.apiPost.consultarTodos()
		});
	}

	render() {
		//console.log(this.state.posts);
		const { posts } = this.state;
		return (
			<div>
				<table className="table">
					<thead>
						<tr>
							<th>Titulo</th>
							<th>Fecha de Publicación</th>
						</tr>
					</thead>
					<tbody>
						{posts.map((item) => {
							return (
								<tr key={item.idPost}>
									<td>{item.titulo}</td>
									<td>{item.fechaPublicacion}</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	}
}

export default ConsultarPost;

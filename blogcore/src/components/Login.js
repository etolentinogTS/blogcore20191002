import React, { Component } from 'react';
import APIAuth from '../api/APIAuth';

export class Login extends Component {
	state = {
		loading: false,
		loginvm: {
			userName: '',
			password: ''
		}
	};

	apiAuth = new APIAuth();

	iniciarSesion = async (e) => {
		e.preventDefault();
		this.setState({
			loading: true
		});

		await this.apiAuth.login(this.state.loginvm);
		// if (await this.apiAuth.login(this.state.loginvm)) {
		// 	alert('Sesión iniciada con éxito');
		// }

		this.setState({
			loading: false,
			loginvm: {
				userName: '',
				password: ''
			}
		});
	};

	textChange = (e) => {
		// BONITA / REACT
		const { name, value } = e.target;
		this.setState({
			loginvm: {
				...this.state.loginvm,
				[name]: value
			}
		});
	};

	render() {
		const { loading } = this.state;
		const { userName, password } = this.state.loginvm;
		return (
			<div className="container">
				<div className="column is-6 is-offset-3">
					<h2 className="title is-2 has-text-centered">BlogCore</h2>
					<form className="box" onSubmit={this.iniciarSesion}>
						<div className="field">
							<label className="label">Usuario</label>
							<input
								className="input"
								type="text"
								name="userName"
								value={userName}
								onChange={this.textChange}
							/>
						</div>
						<div className="field">
							<label className="label">Contraseña</label>
							<input
								className="input"
								type="password"
								name="password"
								value={password}
								onChange={this.textChange}
							/>
						</div>
						<button type="submit" className={'button is-primary ' + (loading ? 'is-loading' : '')}>
							Entrar
						</button>
					</form>
				</div>
			</div>
		);
	}
}

export default Login;

﻿// <auto-generated />
using System;
using BlogCore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BlogCore.Migrations
{
    [DbContext(typeof(BlogCoreContext))]
    [Migration("20190912012004_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BlogCore.Models.Comentario", b =>
                {
                    b.Property<int>("idComentario")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("fechaPublicacion");

                    b.Property<int>("idPost");

                    b.Property<int>("idUsuario");

                    b.Property<string>("mensaje")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("idComentario");

                    b.HasIndex("idPost");

                    b.HasIndex("idUsuario");

                    b.ToTable("Comentarios");
                });

            modelBuilder.Entity("BlogCore.Models.Post", b =>
                {
                    b.Property<int>("idPost")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("cuerpo")
                        .IsRequired();

                    b.Property<DateTime>("fechaPublicacion");

                    b.Property<int>("idUsuario");

                    b.Property<string>("titulo")
                        .IsRequired();

                    b.HasKey("idPost");

                    b.HasIndex("idUsuario");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("BlogCore.Models.Usuario", b =>
                {
                    b.Property<int>("idUsuario")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("avatar");

                    b.Property<string>("email")
                        .IsRequired();

                    b.Property<string>("password")
                        .IsRequired();

                    b.Property<string>("userName")
                        .IsRequired();

                    b.HasKey("idUsuario");

                    b.ToTable("Usuarios");
                });

            modelBuilder.Entity("BlogCore.Models.Comentario", b =>
                {
                    b.HasOne("BlogCore.Models.Post", "post")
                        .WithMany("comentarios")
                        .HasForeignKey("idPost")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("BlogCore.Models.Usuario", "usuario")
                        .WithMany("comentarios")
                        .HasForeignKey("idUsuario")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("BlogCore.Models.Post", b =>
                {
                    b.HasOne("BlogCore.Models.Usuario", "usuario")
                        .WithMany("posts")
                        .HasForeignKey("idUsuario")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}

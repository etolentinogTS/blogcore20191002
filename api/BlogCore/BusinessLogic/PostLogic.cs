﻿using System;
using System.Collections.Generic;
using BlogCore.Models;
using BlogCore.Repository;

namespace BlogCore.BusinessLogic
{
    public class PostLogic
    {
        private PostRepository postRepository;
        private AuthLogic authLogic;

        public PostLogic()
        {
            postRepository = new PostRepository();
            authLogic = new AuthLogic();
        }

        public void agregar(Post post, string token)
        {
            try
            {
                validarPost(post);

                //Consultemos el usario en sesión
                Usuario usuarioSesion = authLogic.consultarUsuarioSesion(token);

                //asignemos el id del usuario en sesión al post que se va a agregar
                post.idUsuario = usuarioSesion.idUsuario;
                post.fechaPublicacion = DateTime.Now;

                postRepository.agregar(post);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void validarPost(Post post)
        {
            try
            {
                Post porTitulo = postRepository.consultarPorTitulo(post.titulo);

                if(porTitulo != null)
                {
                    throw new Exception("Ya existe un Post con este título.");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<Post> consultarTodos()
        {
            try
            {
                return postRepository.consultarTodos();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Post consultarPorId(int idPost)
        {
            try
            {
                Post post = postRepository.consultarPorId(idPost);

                if(post == null)
                {
                    throw new Exception("Post no encontrado");
                }

                return post;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

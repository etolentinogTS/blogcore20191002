﻿using System;
using System.Collections.Generic;
using BlogCore.Models;
using BlogCore.Repository;

namespace BlogCore.BusinessLogic
{
    public class ComentarioLogic
    {
        private ComentarioRepository comentarioRepository;
        private PostLogic postLogic;
        private AuthLogic authLogic;

        public ComentarioLogic()
        {
            comentarioRepository = new ComentarioRepository();
            postLogic = new PostLogic();
            authLogic = new AuthLogic();
        }

        public List<Comentario> consultarPorIdPost(int idPost)
        {
            try
            {
                postLogic.consultarPorId(idPost);

                return comentarioRepository.consultarPorIdPost(idPost);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void agregar(Comentario comentario, string token)
        {
            try
            {
                validarComentario(comentario);

                Usuario usuario = authLogic.consultarUsuarioSesion(token);
                comentario.idUsuario = usuario.idUsuario;
                comentario.fechaPublicacion = DateTime.UtcNow;
                comentarioRepository.agregar(comentario);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public void validarComentario(Comentario comentario)
        {
            try
            {
                postLogic.consultarPorId(comentario.idPost);

                if (comentario.mensaje.Length > 100)
                {
                    throw new Exception("El mensaje debe ser máximo 100 caracteres");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

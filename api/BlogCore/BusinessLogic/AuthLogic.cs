﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BlogCore.Models;
using BlogCore.Models.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using BlogCore.Repository;
using System.Linq;

namespace BlogCore.BusinessLogic
{
    public class AuthLogic
    {
        private readonly IConfiguration _configuration;
        private UsuarioRepository usuarioRepository;

        public AuthLogic(IConfiguration configuration) 
        {
            _configuration = configuration;
            usuarioRepository = new UsuarioRepository();
        }

        public AuthLogic()
        {
            usuarioRepository = new UsuarioRepository();
        }

        private Usuario validarLogin(LoginViewModel login)
        {
            try
            {
                //Consultar el usuario en la base de datos por su email
                Usuario usuario = usuarioRepository.consultarPorUserName(login.userName);

                if (usuario != null)
                {
                    if (usuario.password == login.password)
                    {
                        return usuario;
                    }
                }

                throw new Exception("Usuario o contraseña inválida.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string generarTokenSession(LoginViewModel login)
        {
            try
            {
                Usuario usuario = validarLogin(login);

                // Leemos el secret_key desde nuestro appseting
                var secretKey = _configuration.GetValue<string>("SecretKey");
                var key = Encoding.ASCII.GetBytes(secretKey);

                // Creamos los claims (pertenencias, características) del usuario

                var claims = new ClaimsIdentity();
                claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, usuario.idUsuario.ToString()));
                claims.AddClaim(new Claim(ClaimTypes.Email, usuario.userName));

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = claims,
                    // Nuestro token va a durar una hora
                    Expires = DateTime.UtcNow.AddHours(1),
                    // Credenciales para generar el token usando nuestro secretykey y el algoritmo hash 256
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var createdToken = tokenHandler.CreateToken(tokenDescriptor);

                return tokenHandler.WriteToken(createdToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuario consultarUsuarioSesion(string token)
        {
            token = token.Substring(7);

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken jsonToken = handler.ReadToken(token) as JwtSecurityToken;

            string idUsuarioString = jsonToken.Claims.FirstOrDefault(claim => claim.Type == "nameid").Value;
            int idUsuario = 0;
            bool idValido = int.TryParse(idUsuarioString, out idUsuario);

            if (idValido)
            {
                Usuario usuarioSesion = usuarioRepository.consultarPorId(idUsuario);
                return (usuarioSesion != null) ? usuarioSesion : throw new Exception("Inicie sesión");
            }

            throw new Exception("Inicie sesión");
        }

        public UsuarioViewModel consultarSesion(string token)
        {
            token = token.Substring(7);

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken jsonToken = handler.ReadToken(token) as JwtSecurityToken;

            string idUsuarioString = jsonToken.Claims.FirstOrDefault(claim => claim.Type == "nameid").Value;
            int idUsuario = 0;
            bool idValido = int.TryParse(idUsuarioString, out idUsuario);

            if (idValido)
            {
                Usuario usuarioSesion = usuarioRepository.consultarPorId(idUsuario);

                if(usuarioSesion != null)
                {
                    return new UsuarioViewModel
                    {
                        userName = usuarioSesion.userName,
                        avatar = usuarioSesion.avatar
                    };
                }
            }

            throw new Exception("Inicie sesión");
        }
    }
}

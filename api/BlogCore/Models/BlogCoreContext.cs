﻿using System;
using Microsoft.EntityFrameworkCore;

namespace BlogCore.Models
{
    public class BlogCoreContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=BlogCore;User Id=sa;\nPassword=Tolentino1;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configurar Las tablas de la base de datos
            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(usuario => usuario.idUsuario);
                entity.Property(usuario => usuario.idUsuario).UseSqlServerIdentityColumn();

                entity.Property(usuario => usuario.userName).IsRequired();
                entity.Property(usuario => usuario.email).IsRequired();
                entity.Property(usuario => usuario.password).IsRequired();
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.HasKey(post => post.idPost);
                entity.Property(post => post.idPost).UseSqlServerIdentityColumn();

                entity.Property(post => post.titulo).IsRequired();
                entity.Property(post => post.cuerpo).IsRequired();
                entity.Property(post => post.fechaPublicacion).IsRequired();
                entity.Property(post => post.idUsuario).IsRequired();

                entity.HasOne(post => post.usuario)
                    .WithMany(usuario => usuario.posts)
                    .HasForeignKey(post => post.idUsuario)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Comentario>(entity =>
            {
                entity.HasKey(comentario => comentario.idComentario);
                entity.Property(comentario => comentario.idComentario).UseSqlServerIdentityColumn();

                entity.Property(comentario => comentario.fechaPublicacion).IsRequired();
                //Dato cambiado
                entity.Property(comentario => comentario.mensaje)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(comentario => comentario.idUsuario).IsRequired();
                entity.Property(comentario => comentario.idPost).IsRequired();

                entity.HasOne(comentario => comentario.usuario)
                    .WithMany(usuario => usuario.comentarios)
                    .HasForeignKey(post => post.idUsuario)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(comentario => comentario.post)
                    .WithMany(post => post.comentarios)
                    .HasForeignKey(post => post.idPost)
                    .OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}

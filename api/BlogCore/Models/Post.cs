﻿using System;
using System.Collections.Generic;

namespace BlogCore.Models
{
    public class Post
    {
        public int idPost { get; set; }
        public string titulo { get; set; }
        public string cuerpo { get; set; }
        public DateTime fechaPublicacion { get; set; }
        public int idUsuario { get; set; }

        public virtual Usuario usuario { get; set; }
        public virtual ICollection<Comentario> comentarios { get; set; }
    }
}

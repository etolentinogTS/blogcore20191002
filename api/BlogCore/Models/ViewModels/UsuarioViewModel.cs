﻿using System;
namespace BlogCore.Models.ViewModels
{
    public class UsuarioViewModel
    {
        public string userName { get; set; }
        public string avatar { get; set; }
    }
}

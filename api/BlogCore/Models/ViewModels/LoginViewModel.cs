﻿using System;
namespace BlogCore.Models.ViewModels
{
    public class LoginViewModel
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}

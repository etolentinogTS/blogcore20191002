﻿using System;
using System.Collections.Generic;

namespace BlogCore.Models
{
    public class Usuario
    {
        public int idUsuario { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string avatar { get; set; }

        public virtual ICollection<Post> posts { get; set; }
        public virtual ICollection<Comentario> comentarios { get; set; }
    }
}

﻿using System;
namespace BlogCore.Models
{
    public class Comentario
    {

        public int idComentario { get; set; }
        public string mensaje { get; set; }
        public DateTime fechaPublicacion { get; set; }
        public int idUsuario { get; set; }
        public int idPost { get; set; }

        public virtual Usuario usuario { get; set; }
        public virtual Post post { get; set; }
    }
}

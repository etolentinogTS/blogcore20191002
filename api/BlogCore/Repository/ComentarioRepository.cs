﻿using System;
using BlogCore.Models;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace BlogCore.Repository
{
    public class ComentarioRepository
    {
        private BlogCoreContext db;

        public ComentarioRepository()
        {
            db = new BlogCoreContext();
        }

        public List<Comentario> consultarPorIdPost(int idPost)
        {
            try
            {
                return db.Comentarios
                    .Include(comentario => comentario.usuario)
                    .Where(comentario => comentario.idPost == idPost)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void agregar(Comentario comentario)
        {
            try
            {
                db.Comentarios.Add(comentario);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
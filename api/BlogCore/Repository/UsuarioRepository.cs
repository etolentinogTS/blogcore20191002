﻿using System;
using System.Linq;
using BlogCore.Models;

namespace BlogCore.Repository
{
    public class UsuarioRepository
    {
        private BlogCoreContext db;
        public UsuarioRepository()
        {
            db = new BlogCoreContext();
        }

        public Usuario consultarPorUserName(string userName)
        {
            return db.Usuarios.FirstOrDefault(u => u.userName == userName);
        }

        public Usuario consultarPorId(int idUsuario)
        {
            return db.Usuarios.FirstOrDefault(usuario => usuario.idUsuario == idUsuario);
        }
    }
}

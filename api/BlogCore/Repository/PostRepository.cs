﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlogCore.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogCore.Repository
{
    public class PostRepository
    {
        private BlogCoreContext db;
        public PostRepository()
        {
            db = new BlogCoreContext();
        }

        public void agregar(Post post)
        {
            try
            {
                db.Posts.Add(post);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Post> consultarTodos()
        {
            try
            {
                return db.Posts.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Post consultarPorId(int idPost)
        {
            try
            {
                return db.Posts
                    .Include(post=> post.usuario)
                    .Include(post => post.comentarios)
                        .ThenInclude(comentario => comentario.usuario)
                    .FirstOrDefault(post => post.idPost == idPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Post consultarPorTitulo(string titulo)
        {
            try
            {
                return db.Posts.FirstOrDefault(post => post.titulo == titulo);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

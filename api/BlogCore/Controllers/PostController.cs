﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BlogCore.BusinessLogic;
using BlogCore.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogCore.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        PostLogic postLogic;
        public PostController()
        {
            postLogic = new PostLogic();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult consultarTodos()
        {
            try
            {
                return Json(postLogic.consultarTodos());
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult consultarPorId(int idPost)
        {
            try
            {
                return Json(postLogic.consultarPorId(idPost));
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult agregar([FromBody]Post post)
        {
            try
            {
                postLogic.agregar(post, Request.Headers["Authorization"].First());
                return Ok("Post publicado exitosamente");
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}

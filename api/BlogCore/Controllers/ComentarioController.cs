﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BlogCore.BusinessLogic;
using BlogCore.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogCore.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ComentarioController : Controller
    {
        ComentarioLogic comentarioLogic;
        public ComentarioController()
        {
            comentarioLogic = new ComentarioLogic();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult consultarPorIdPost(int idPost)
        {
            try
            {
                return Json(comentarioLogic.consultarPorIdPost(idPost));
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpPost]
        [Route("[action]")]
        public IActionResult agregar([FromBody]Comentario comentario)
        {
            try
            {
                comentarioLogic.agregar(comentario, Request.Headers["Authorization"].First());
                return Ok("Comentario agregado exitosamente");

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}

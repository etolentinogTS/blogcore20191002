﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BlogCore.BusinessLogic;
using BlogCore.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogCore.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private AuthLogic authLogic;
        public AuthController(IConfiguration configuration)
        {
            authLogic = new AuthLogic(configuration);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Login([FromBody]LoginViewModel login)
        {
            try
            {
                return Json(new TokenViewModel { token = authLogic.generarTokenSession(login) });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("[action]")]
        public IActionResult consultarUsuarioSesion()
        {
            try
            {
                return Json(authLogic.consultarSesion(Request.Headers["Authorization"].First()));
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
